<?php
/**
 * Created by Stefan Ho.
 * User: Stefan <xiugang.he@skyee360.com>
 * Date: 2021-07-02 08:55
 */

/**
 * 代码调试
 */
function p() {
    $argc = func_get_args();
    echo '<pre>';
    foreach($argc as $var) {
        print_r($var);
        echo '<br/>';
    }
    echo '</pre>';
    exit;
}

/**
 * 代码调试
 */
function pr() {
    $argc = func_get_args();
    echo '<pre>';
    foreach($argc as $var) {
        print_r($var);
        echo '<br/>';
    }
    echo '</pre>';
}

/**
 * @param $arr
 * @param $key
 * @param string $default
 * @return mixed|string
 */
function array_value($arr, $key, $default = '') {
    $keys = explode('.', $key);
    $data = $arr;
    foreach($keys as $one_key) {
        if((is_array($data) || $data instanceof ArrayAccess) && isset($data[$one_key])) {
            $data = $data[$one_key];
        } else {
            return $default;
        }
    }

    return $data;
}

/**
 * 发送邮件
 * @param $config
 * @param $to
 * @param $title
 * @param $content
 * @param string $attachments
 * @return bool
 * @throws phpmailerException
 */
function send_mail($config, $to, $title, $content, $attachments = '') {
    include_once ROOT_PATH . './libraries/phpmailer/class.phpmailer.php';
    $mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->IsHTML();
    $mail->SMTPAuth = true;
    $mail->Host = $config['host'];
    $mail->Port = $config['port'];
    $mail->CharSet = $config['charset'];
    $mail->Username = $config['user'];
    $mail->Password = $config['password'];
    $mail->From = $config['user'];
    $mail->FromName = $config['formname'];
    $mail->Subject = $title;
    $mail->Body = $content;
    if(!is_array($to)) {
        $to = array_filter(explode(',', $to));
    }
    if(!$to) {
        return false;
    }
    foreach($to as $one) {
        $mail->AddAddress($one);
    }
    $to_admin = $config['admin'];
    if(!is_array($to_admin)) {
        $to_admin = array_filter(explode(',', $to_admin));
    }
    foreach($to_admin as $one) {
        $mail->AddCC($one);
    }
    if(!is_array($attachments)) {
        $attachments = explode(',', $attachments);
    }
    foreach($attachments as $attachment) {
        if(file_exists($attachment)) {
            $mail->AddAttachment($attachment);
        }
    }

    return $mail->Send();
}
