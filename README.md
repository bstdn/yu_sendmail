# 邮件自动发送项目

> Stefan阿钢

## 如何使用

1. 拷贝`config.example.php`并命名为`config.php`，修改配置内容
2. 创建数据库及表

```mysql
CREATE TABLE `ck1_user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL DEFAULT '' COMMENT '姓名',
  `job_time` date NOT NULL COMMENT '入职时间',
  `email` varchar(20) NOT NULL DEFAULT '' COMMENT '邮箱地址',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
```

3. 录入相应记录
4. 创建定时任务
    1. Windows可使用计划任务
    2. Linux可使用`crontab`

```
#每天10点执行
* 10 * * * php sendmail.php
```
