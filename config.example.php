<?php
/**
 * Created by Stefan Ho.
 * User: Stefan <xiugang.he@skyee360.com>
 * Date: 2021-07-02 09:27
 */

$_config = [];
// CONFIG DB
$_config['db']['1']['dbhost'] = 'localhost';
$_config['db']['1']['dbuser'] = 'root';
$_config['db']['1']['dbpw'] = '123456';
$_config['db']['1']['dbcharset'] = 'utf8';
$_config['db']['1']['pconnect'] = '0';
$_config['db']['1']['dbname'] = 'sendmail';
$_config['db']['1']['tablepre'] = 'pre_';
// 邮箱服务
$_config['mail_server']['host'] = 'smtp.exmail.qq.com';
$_config['mail_server']['port'] = '25';
$_config['mail_server']['charset'] = 'UTF-8';
$_config['mail_server']['user'] = '';
$_config['mail_server']['password'] = '';
$_config['mail_server']['formname'] = '系统';
// 邮件同步发送给管理员，多个用英文逗号分隔
$_config['mail_server']['admin'] = '';
$_config['mail_server']['title'] = '';
$_config['mail_server']['content'] = '';
